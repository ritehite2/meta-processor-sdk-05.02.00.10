require linux-processor-sdk-4.14.inc

PR_append = ".9"

KERNEL_GIT_URI = "git://git.ti.com/processor-sdk/processor-sdk-linux.git"
BRANCH = "processor-sdk-linux-rt-4.14.y"
SRCREV = "a72bf1418cb2c531ca7f06989917acc0cbd5a044"
