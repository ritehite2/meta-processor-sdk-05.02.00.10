require linux-processor-sdk-4.14.inc

PR_append = ".9"

KERNEL_GIT_URI = "git://git.ti.com/processor-sdk/processor-sdk-linux.git"
BRANCH = "processor-sdk-linux-4.14.y"
SRCREV = "e669d52447df61f9d7b8ef72c9f22f4feed04d38"
